#!/usr/bin/env gnuplot

set key noautotitle
set samples 200
set xrange [0:2]
set xzeroaxis
unset xtics
unset ytics

y(x) = 1/(1-x)
set label '(t_0+1/y_0)' at 1,0 point lt 1 pt 2 ps 3 offset 1,1
plot (x<1?y(x):1/0), (x>1?y(x):1/0)
